# Changelog
What's new? What's up? You'll find it here!<br>
*Note: dates are in MM/DD/YYYY format.*

## 0.0.2 [07/03/2021]
* **Migrated to TypeScript! Very exciting!**
* Created `User` class
  * Contains an address (format: `sobi-<UUIDv4>`), a username, an email, a public/private key pair, and a revocation certificate in case the private key is leaked.
* Created `Transaction` class
  * Contains the user who sent it, who recieved it, what the amount was, and the signature that verifies the transaction via a timestamp and a nonce ([`Crypto.randomBytes(16)`](https://nodejs.org/docs/latest-v14.x/api/crypto.html#crypto_crypto_randombytes_size_callback))
* Created `Block` class
  * The "node" in a [doubly-linked list](https://en.wikipedia.org/wiki/Doubly_linked_list). Which is really just all a blockchain is- a fancy doubly-linked list with a bit more work.
  * Contains a list of `Transaction`s.
* Created `Blockchain` class
  * The helper class that facilitates linking `Block`s in a doubly-linked list.
  * Will eventually import from a JSON file or a database or something
* `util/`s:
  * `createTransaction.ts`: Shortcut function that creates a signature for a transaction, creates a new signed `Transaction` instance, and returns it
  * `createUser.ts`: Shortcut function that creates a public/private key pair (+ the revocation cert) for a given username, email, and passphrase, then creates and returns a `User` instance with the required fields filled out.
* **Still to do:**
  * Convert `src-old/` to TypeScript, sort them into their appropriate files, maybe rewrite a section or two. We'll see.
  * Fancy colored logging. I already have the util class for it, I just haven't gotten around to using it.

## 0.0.1
* No data, unfortunately