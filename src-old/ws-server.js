console.to   = (...args) => console.log('[CLIENT => WS]', ...args);
console.from = (...args) => console.log('[WS => CLIENT]', ...args);

const WebSocket = require('ws');
// Set up the .env file
require('dotenv').config({ path: '../.env' });

const SERVER_PORT = process.env.SERVER_PORT || 42069;
const wss = new WebSocket.Server({ port: SERVER_PORT });
wss.on('listening', () => console.log(`Server open and listening on port ${SERVER_PORT}`));

wss.on('connection', (ws, req) => {
  console.from(`New connection from ${req.connection.remoteAddress}`);

  console.to('Sending identify, awaiting response');
  const connectionTimer = setTimeout(() => ws.close(1000, 'Client took too long to identify'), 3000);

  // Send the client an identify packet to let them know that they have connected
  ws.send(JSON.stringify({ action: 'identify' }));

  ws.on('message', message => {
    // The data will be a string when it comes through. Parse it into an object.
    message = JSON.parse(message);

    console.from(message);

    // If the client isn't identified and they try to make a request other than identify, close it.
    if (!req.isIdentified && message.action !== 'identify') ws.close(1000, 'Must identify first');

    if (!req.isIdentified) {
      // First check if they have a key and it's correct
      if (!message.key || message.key !== process.env.WEBSOCKET_KEY) ws.close(1000, 'Invalid websocket key');

      switch (message.type) {
        // If the client is a miner
        case 'miner': {
          clearTimeout(connectionTimer);
          req.isIdentified = true;
          req.type = 'miner';

          ws.send(JSON.stringify({ action: 'identify_OK', type: 'miner' }));
          console.from('Client identified: miner');
          break;
        }
        // If the client is just a basic client that's listening for blocks
        case 'client': {
          clearTimeout(connectionTimer);
          req.isIdentified = true;
          req.type = 'client';

          ws.send(JSON.stringify({ action: 'identify_OK', type: 'client' }));
          break;
        }
        default: ws.close(1000, 'Invalid type');
      }
    }
  });

  ws.on('close', (code, reason) => {
    if (code === 1006) reason = 'Broken connection';
    console.from('Connection closed');
    console.from(`Code: ${code} | Reason: ${reason}`);
  });

});