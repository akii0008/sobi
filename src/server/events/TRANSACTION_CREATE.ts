import { WebSocket } from "ws";
import Transaction, { ITransaction } from "../../common/classes/Transaction";
import IncomingServerEvent from "../../common/interfaces/IncomingServerEvent";
import ServerEventClass from "../../common/interfaces/ServerEventClass";
import isInterface from "../../common/util/isInterface";

export default class TRANSACTION_CREATE extends ServerEventClass {

  public run(ws: WebSocket, data: IncomingServerEvent["d"]): boolean {
    if (!isInterface<ITransaction>(data, "signature")) return false;

    const txData: ITransaction = data;
    const transaction = new Transaction(txData);

    console.log("WE GOT A TRANSACTION", transaction.toString());

    return true;
  }

}