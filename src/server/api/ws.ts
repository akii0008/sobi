import WebSocket from "ws";
import { EventEmitter } from "events";
import { readdirSync } from "fs";

import ops from "../../common/interfaces/OPCODES";
import logger from "../../common/util/logger";
import config from "../../config";
import ServerEventClass from "../../common/interfaces/ServerEventClass";
import IncomingServerEvent from "../../common/interfaces/IncomingServerEvent";

// https://www.typescriptlang.org/docs/handbook/utility-types.html#constructorparameterstype
type EventEmitterOptions = ConstructorParameters<typeof EventEmitter>[0];

export default class Server extends EventEmitter {

  public server: WebSocket.Server;
  private events: Map<string, ServerEventClass>;

  constructor(options?: EventEmitterOptions) {
    super(options);

    const wss = this.server = new WebSocket.Server({ port: config.port });
    this.events = new Map();

    const events = readdirSync("./build/src/server/events/");
    events.forEach(async eventFile => {
      const eventName = eventFile.split(".js")[0];
      logger.verbose(`Importing websocket event ${eventName}`);

      const event = await import(`../events/${eventFile}`);

      const eventClass: ServerEventClass = new event.default();
      this.events.set(eventName, eventClass);
    });
    logger.log("Finished loading events");
    logger.log("Ready for commands");

    wss.on("listening", () => logger.log(`WebSocket server listening on port ${config.port}`));
    wss.on("connection", (ws, req) => {
      logger.log(`New connection: ${req.socket.remoteAddress}`);

      ws.on("message", (data: Buffer) => {
        const message: IncomingServerEvent = JSON.parse(data.toString("utf8"));

        logger.verbose(`New message! Opcode ${ops[message.op]}`);

        if (!ops[message.op]) {
          logger.warn(`Received invalid opcode ${message.op}`);
          return ws.close(undefined, JSON.stringify({ op: ops.INVALID_REQUEST }));
        }

        const event = this.events.get(ops[message.op]);
        if (!event) {
          return logger.warn(`Missing event file for op code ${message.op}`);
        }
        event.run(ws, message.d);
      });
    });


  }

}