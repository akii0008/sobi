import Block from "./Block";

export default class Blockchain {

  public blocks: Block[];
  public head: Block;
  public tail: Block;

  constructor(blocks: Block[]) {
    this.blocks = blocks;

    this.head = this.blocks[0];
    this.tail = this.blocks[this.blocks.length - 1];

    for(let i = 0; i < this.blocks.length; i++) {
      if(this.blocks[i - 1]) this.blocks[i].setParent(this.blocks[i - 1]);
      if(this.blocks[i + 1]) this.blocks[i].setChild(this.blocks[i + 1]);
    }
  }

  public toString(): string {
    return `Blockchain; Blocks: ${this.blocks.length}; Head hash: ${this.head?.hash ?? null}; Tail hash: ${this.tail?.hash ?? null}`;
  }

  public push(newBlock: Block): Block {
    if(newBlock.status !== "mined") throw new Error("Block must be mined first before pushing to blockchain.");

    this.tail?.setChild(newBlock);
    newBlock.setParent(this.tail);
    this.blocks.push(newBlock);
    this.tail = newBlock;

    return newBlock;
  }

  public json(): string {
    const toStringify = [];

    for(const block of this.blocks) {
      // we're pushing hashes instead of whole block objects because it'll error
      // (as a circular reference error- cant convert those into json)
      toStringify.push({
        prevHash: block.parent?.hash ?? null,
        currentHash: block.hash,
        nextHash: block.child?.hash ?? null,
        coinbase: block.coinbase,
        transactions: block.transactions
      });
    }

    const stringified = JSON.stringify(toStringify, (k, v) => {
      // filter out private properties
      if(["privateKey", "revocationCertificate"].includes(k)) return undefined;
      else return v;
    });

    return stringified;
  }

}