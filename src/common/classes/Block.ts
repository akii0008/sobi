import Transaction from "./Transaction";
import Coinbase from "./Coinbase";
import User from "./User.js";
import config from "../../config";
import { cpus } from "os";
import { Worker } from "worker_threads";
import logger from "../util/logger";

interface IBlock {
  coinbase: Coinbase
  transactions: Transaction[] | null
}

export type BlockStatus = "mined" | "unmined" | "invalid";

export default class Block {

  public readonly coinbase: Coinbase;
  public transactions: Transaction[] | null = null;
  public parent: Block | null = null;
  public child: Block | null = null;

  // internally generated
  public reward: number | null = null;
  public nonce: number | null = null;
  public hash: string | null = null;
  public minedBy: User | null = null;

  private _status: BlockStatus;

  constructor({ coinbase, transactions }: IBlock) {
    this.coinbase = coinbase;
    this.transactions = transactions ?? null;

    this.reward = this.transactions?.map(g => g.fee).reduce((acc, curr) => acc + curr) ?? null;

    this._status = "unmined";
  }

  public toString(): string {
    return `Block; Parent hash: ${this.parent?.hash ?? "0"}; ${this.transactions?.length ?? 0} transactions.`;
  }

  /**
   * Sets the child block of this block
   * @param {Block} newChild The new child block to set
   * @returns {Block} The current block (this)
   */
  public setChild(newChild: Block): Block {
    this.child = newChild;
    return this;
  }

  /**
   * Sets the parent block of this block
   * @param {Block} newParent The new parent block to set
   * @returns {Block} The current block (this)
   */
  public setParent(newParent: Block): Block {
    this.parent = newParent;
    return this;
  }

  public addTransaction(transaction: Transaction): Transaction {
    if (this._status === "mined")
      throw new Error("Block has already been mined. No new transactions can be added.");
    else if (this._status === "invalid")
      throw new Error("Block is invalid. No new transactions can be added.");

    if (this.transactions === null) {
      this.transactions = [];
    }
    this.transactions.push(transaction);
    return transaction;
  }

  public mine(miner: User, previousBlock: Block | null, outputInterval = 1000, threads = cpus().length): Promise<number> {
    // wrap this in a promise since it's not an immediate output thing.
    // this is different than making the function async because we need resolve/reject
    return new Promise((resolve, reject) => {
      let nonce = 0;
      let hash: string;

      const workers: Worker[] = [];

      for (let i = 0; i < threads; i++) {
        // worker data to pass
        const workerData = {
          difficulty: config.difficulty, // footnote [1]
          outputInterval,
          nonce: i + threads,
          coinbase: this.coinbase,
          prevHash: previousBlock?.hash ?? "0", // nullish '0' because this could be the genesis block.
          transactions: this.transactions,
          threads
        };

        // spawn a worker thread to keep the CPU-intensive task of mining off the main thread
        const worker = new Worker("./build/src/common/util/mine.js", {
          workerData
        });

        workers.push(worker);

        worker.on("online", () => logger.log(`Worker ${i} online. Mining...`));

        // we'll get a message every (nonce % outputInterval) cycles containing useful info
        // update the nonce and hash accordingly.
        // we'll get one last message (when the script has exited the while loop)
        // containing the final nonce and hash.
        worker.on("message", message => {
          nonce = message.nonce;
          hash = message.hash;
          logger.verbose(`[WORKER ${i}]: nonce: ${message.nonce} hash: ${message.hash}`);
        });
        // if there was an error, reject the promise. obviously.
        worker.on("error", reject);

        worker.on("exit", code => {
          if (code !== 0) return reject(`Mining worker exited with non-zero code: ${code}`);
          else {
            // update all the necessary values lol
            this.nonce = nonce;
            this.hash = hash;
            this.minedBy = miner;

            this.transactions?.forEach(transaction => transaction.confirm());
            
            this._status = "mined";

            workers.forEach(w => { w.terminate(); });

            // and resolve with the final nonce.
            return resolve(nonce);
          }
        });
      }

    });
  }

  /**
   * Mark this block as invalid.
   * This can happen for a variety of reasons, the most likely of which being there was an invalid transaction.
   */
  public invalidate(): void {
    if (this._status !== "unmined")
      throw new Error(`This block already has status of ${this._status}`);

    this._status = "invalid";
  }

  get status(): BlockStatus {
    return this._status;
  }

}

/*
[1] We're passing the difficulty here instead of importing it in the mine.ts script because
    the difficulty may change while the program is running. By contrast, the value in the
    config file is static- it will never change unless i update it on the disk- so the
    difficulty will always remain the same.
*/