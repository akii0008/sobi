import User from "./User";
import { DateTime } from "luxon";
import Address from "../interfaces/Address";

export interface ITransaction {
  from: User,
  to: Address,
  amount: number,
  fee: number,
  signature: string
}

export type TransactionStatus = "confirmed" | "unconfirmed" | "invalid";

export default class Transaction {

  public readonly from: User;
  public readonly to: Address;
  public readonly amount: number;
  public readonly fee: number;
  public readonly signature: string;
  private _status: TransactionStatus;

  // internally generated
  public readonly timestamp: Date;

  constructor({ from, to, amount, fee, signature }: ITransaction) {
    this.from = from;
    this.to = to;
    this.amount = amount;
    this.fee = fee;
    this.signature = signature;

    this.timestamp = new Date();

    this._status = "unconfirmed";
  }

  public json(): string {
    return JSON.stringify({
      from: this.from,
      to: this.to,
      amount: this.amount,
      fee: this.fee,
      signature: this.signature,
      timestamp: this.timestamp,
      status: this.status
    });
  }

  public toString(): string {
    return `${this.from.toString()} sent ${this.amount} SOBI to ${this.to} on ${DateTime.fromJSDate(this.timestamp).toFormat("yyyy-LL-dd HH:mm:ss")}`;
  }

  /**
   * Mark this transaction as confirmed and validated by a miner.
   */
  public confirm(): void {
    if (this.status !== "unconfirmed")
      throw new Error(`This transaction already has status of ${this._status}`);

    this._status = "confirmed";
  }

  /**
   * Mark this transaction as invalid.
   * This can happen for a variety of reasons, the most likely of which being the sender sent more than they had.
   */
  public invalidate(): void {
    if (this.status !== "unconfirmed")
      throw new Error(`This transaction already has status of ${this._status}`);
    
    this._status = "invalid";
  }

  get status(): TransactionStatus {
    return this._status;
  }

}