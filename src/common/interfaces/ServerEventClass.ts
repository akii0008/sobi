import { WebSocket } from "ws";
import IncomingServerEvent from "./IncomingServerEvent";

export default abstract class ServerEventClass {

  public abstract run(ws: WebSocket, data: IncomingServerEvent["d"]): boolean;

}