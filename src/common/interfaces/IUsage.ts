import { ArgumentOptions } from "bandersnatch";

export default interface IUsage {
  name: string,
  options: ArgumentOptions
}