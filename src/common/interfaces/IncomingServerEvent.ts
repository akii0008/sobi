import OPCODES from "./OPCODES";

export default interface IncomingServerEvent {
  op: OPCODES,
  d: {
    [x: string]: unknown
  }
}