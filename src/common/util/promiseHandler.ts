// This function exists to help us escape the trycatch hell that we javascript developers commonly face
// when awaiting promises.
// Thanks, https://dev.to/ivanz123/say-goodbye-trycatch-hell-336o

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default async function promiseHandler<T>(prom: Promise<T>): Promise<[T | null, any]> {
  try {
    return [await prom, null];
  } catch (error) {
    return [null, error];
  }
}

// Usage:
/* async function
const someOtherPromise = database.findOne({ where: { color: 'blue' } });

const [result, error] = await promiseHandler(someOtherPromise);
if(error)
  throw error;

console.log(result);
*/