/* eslint-disable @typescript-eslint/no-explicit-any */

import createUser from "./createUser";
import logger from "./logger";
import inquirer from "inquirer";
import { writeFileSync } from "fs";

const questions = [
  {
    type: "input",
    name: "email",
    message: "Please enter your email address",
    validate: (input: string): true | string => {
      if (!input) return "You must input an email";
      else return true;
    }
  },
  {
    type: "input",
    name: "username",
    message: "Please enter a username (optional: defaults to email)",
    default: (answersSoFar: { [x: string]: any }) => answersSoFar.email
  },
  {
    type: "password",
    name: "password",
    message: "Please enter a password.\nMust be:\n• At least 8 characters long\n• Have at least 1 uppercase and 1 lowercase character\n• Have at least 1 number\n• Include a special character.\n",
    mask: true,
    validate: (input: string): true | string => {
      if (input.length < 8)
        return "Input length was less than 8";
      if (!(/[A-Z]{1,}/.test(input)))
        return "Input did not include at least one uppercase character";
      if (!(/[a-z]{1,}/.test(input)))
        return "Input did not include at least one lowercase character";
      if (!(/[0-9]{1,}/.test(input)))
        return "Input did not include at least one number";
      if (!(/[!@#$%^&*()_+\-={}|[\]\\:";'<>?,./]{1,}/.test(input)))
        return "Input did not include at least one special character";

      return true;
    }
  },
  {
    type: "password",
    name: "password_confirmation",
    message: "Please re-enter your password",
    mask: true
  }
];

function inquire() {
  inquirer.prompt(questions).then(async answers => {
    const { username, email, password, password_confirmation } = answers;

    if (!email) {
      logger.error("Email is required");
      return inquire();
    }

    if (password !== password_confirmation) {
      logger.error("Passwords do not match.");
      return inquire();
    }

    logger.log("Creating a user...");

    const user = await createUser({ username, email, passphrase: password });
    writeFileSync("./user.json", user.json(), "utf8");

    logger.log("User created successfully");

  }).catch(e => {
    throw e;
  });
}

inquire();