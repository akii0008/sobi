import inquirer from "inquirer";
import ReturnCommand from "../../common/interfaces/ReturnCommand";
import IUsage from "../../common/interfaces/IUsage";
import { Arguments } from "bandersnatch";
import { ws, thisUser } from "../index";
import createTransaction from "../../common/util/createTransaction";
import promiseHandler from "../../common/util/promiseHandler";
import logger from "../../common/util/logger";
import Address from "../../common/interfaces/Address";
import OPCODES from "../../common/interfaces/OPCODES";

const questions = [
  {
    type: "password",
    name: "password",
    message: "Please enter your password",
    mask: true
  }
];

// there's gotta be a better way of typing these arguments. for now though this will have to work as i dont understand typescript enough to care at this point
export async function run({ amount, to }: Arguments<{ amount: number, to: Address }>): Promise<ReturnCommand> {
  const { password } = await inquirer.prompt(questions);

  // yeah it's typed but only as a string, not an Address.
  if (!to.startsWith("sobi-"))
    to = `sobi-${to}`;

  const transactionPromise = createTransaction({ from: thisUser, to, amount, fee: 0, passphrase: password });
  const [tx, transactionError] = await promiseHandler(transactionPromise);

  if (transactionError) {
    if (transactionError.message === "Incorrect key passphrase")
      logger.error(transactionError.message);
    else throw transactionError;

    return { success: false };
  }

  ws.send(JSON.stringify({ op: OPCODES.TRANSACTION_CREATE, d: tx }));

  return { success: true };
}

export const name = "send";
export const description = "Send SOBI from one user to another.";

export const usage: IUsage[] = [
  { name: "amount", options: { type: "number", prompt: true } },
  { name: "to", options: { type: "string", prompt: true } },
  { name: "fee", options: { type: "number", prompt: true, optional: true, default: 1 }}
];